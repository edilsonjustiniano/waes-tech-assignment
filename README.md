# WAES Tech assignment

This repository was created to implement the tech assignment required for WAES as part of the recruitment process. It is composed by a Spring boot application with three endpoints. The idea behind this application is receive two binary Base64 requests and compare them

1. Create the right diff
    ```command
    POST /v1/diff/{diff_id}/right
    ```
2. Create the left diff
    ```command
    /v1/diff/{diff_id}/left
    ```
3. Perform the diff between them
    ```command
    /v1/diff/{diff_id}
    ```


Basically, the two first endpoints are pretty similars, the only difference is the attribute where the diff is stored.

Just to remember in order to make the application a little smarter I have included a MongoDB client in order to store the right and left diff. So, it is possible to create the right diff and them re-create it by sending the request in the same endpoint.

The third endpoint perform the diff itself. Check the binaries JSON Base64 received in the first two endpoints and respond with 200 OK for sucess comparison and 400 Bad request with a explained message about the difference. See more details about the diff comparison response below:

Diffs are equal, so the response is 200 OK with the following body:
```json
{
    "diff": 1,
    "right": "<base64 encoded diff>",
    "left": "<base64 encoded diff>"
}
```

For example, the diffs have the same size but different offset, the body response will be:
```json
{
    "message": "The left and right offsets are different.",
    "errorCode": "offset.diff.different"
}
```


## Pre requisites

### Java

> It is required a Java JRE 11 at least to run this application locally

### Gradle

> It is required the gradle on 5.4.1 version at least. To download it, please use the following [link](https://gradle.org/install/).


### How to run it

> Execute the following command on terminal:

```command
gradle clean build
```

> After build the application, just start the application using the command below on terminal:

```command
java -jar  <application_directory>/build/libs/edilson-diff-1.0.0.jar
```

**Note**: Since I am using a embedded mongodb the application take a time to be started for the first time. This is something that I could improve by using the standard mongodb instead. Due this startup issue, please wait for the following message be printed **Tomcat started on port(s): 8080 (http) with context path** and them you will be able to use the endpoints.


### API documentation

> I have implemented the SWAGGER client on this application as well, so It will be easier to test and validate the API. After the application is get up and running, please access the following link: [Swagger API documentation](http://localhost:8080/swagger-ui.html#/diff-controller).

### To be improved

- Use docker to create the micro-service docker image. I did this but due to an issue on startup order I had to go back to the mongodb embedded
- Handle the PUT for create diffs instead of only POST
- Implement the GET operation as well
- Integrate with travis or a Jenkins server as well