package com.waes.techassignment.edilsondiff.api.diff.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Base64;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class DiffParseHelperTest {

    private static final String TEST_STR = "TEST";
    private static final String BASE_64_TEST_STR = Base64.getEncoder().encodeToString(TEST_STR.getBytes());
    private static final byte[] DIFF_RAW_DATA = BASE_64_TEST_STR.getBytes();

    private DiffParseHelper diffParseHelper = new DiffParseHelper();

    @Test
    public void parseDiffValue_shouldParseDiffByteSuccessfully() {
        // when
        String result = diffParseHelper.parseDiffValue(DIFF_RAW_DATA);

        // then
        assertThat(result, equalTo(TEST_STR));
    }
}
