package com.waes.techassignment.edilsondiff.api.diff.service;

import com.waes.techassignment.edilsondiff.api.diff.model.DiffResponse;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffVo;
import com.waes.techassignment.edilsondiff.persistence.diff.model.Diff;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class DiffBuilderTest {

    private static final int DIFF_ID = 1;
    private static final byte[] DIFF_RAW_DATA = new byte[] {2, 1, 'A'};
    private static final String DIFF_PARSED_VALUE = "DIFF";

    @Mock
    private DiffParseHelper diffParseHelper;

    @InjectMocks
    private DiffBuilder diffBuilder;

    @Test
    public void buildDiffVo_shouldBuildADiffVoSuccessfully() {
        // when
        DiffVo result = diffBuilder.buildDiffVo(DIFF_ID, DIFF_RAW_DATA);

        // then
        assertThat(result.getDiffId(), equalTo(DIFF_ID));
        assertThat(result.getRawData(), equalTo(DIFF_RAW_DATA));
    }

    @Test
    public void buildLeftDiff_shouldBuildADiff_withLeftDiffSuccessfully() {
        // give
        DiffVo diffVo = buildDiffVo();
        given(diffParseHelper.parseDiffValue(DIFF_RAW_DATA)).willReturn(DIFF_PARSED_VALUE);

        // when
        Diff result = diffBuilder.buildLeftDiff(diffVo);

        // then
        assertThat(result.getId(), equalTo(DIFF_ID));
        assertThat(result.getLeftRawData(), equalTo(DIFF_RAW_DATA));
        assertThat(result.getLeftParsedData(), equalTo(DIFF_PARSED_VALUE));
    }

    @Test
    public void buildRightDiff_shouldBuildADiff_withRightDiffSuccessfully() {
        // give
        DiffVo diffVo = buildDiffVo();
        given(diffParseHelper.parseDiffValue(DIFF_RAW_DATA)).willReturn(DIFF_PARSED_VALUE);

        // when
        Diff result = diffBuilder.buildRightDiff(diffVo);

        // then
        assertThat(result.getId(), equalTo(DIFF_ID));
        assertThat(result.getRightRawData(), equalTo(DIFF_RAW_DATA));
        assertThat(result.getRightParsedData(), equalTo(DIFF_PARSED_VALUE));
    }

    @Test
    public void buildDiffResponse_shouldBuildADiffResponseSuccessfully() {
        // give
        Diff diff = buildDiff();

        // when
        DiffResponse result = diffBuilder.buildDiffResponse(diff);

        // then
        assertThat(result.getDiff(), equalTo(DIFF_ID));
        assertThat(result.getLeft(), equalTo(DIFF_RAW_DATA));
        assertThat(result.getRight(), equalTo(DIFF_RAW_DATA));
    }

    private DiffVo buildDiffVo() {
        return new DiffVo.Builder()
                .diffId(DIFF_ID)
                .rawData(DIFF_RAW_DATA)
                .build();
    }

    private Diff buildDiff() {
        return new Diff.Builder()
                .id(DIFF_ID)
                .leftRawData(DIFF_RAW_DATA)
                .leftParsedData(DIFF_PARSED_VALUE)
                .rightRawData(DIFF_RAW_DATA)
                .rightParsedData(DIFF_PARSED_VALUE)
                .build();
    }
}
