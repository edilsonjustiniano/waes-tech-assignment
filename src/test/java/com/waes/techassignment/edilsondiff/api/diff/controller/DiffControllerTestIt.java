package com.waes.techassignment.edilsondiff.api.diff.controller;

import com.waes.techassignment.edilsondiff.persistence.diff.model.Diff;
import com.waes.techassignment.edilsondiff.persistence.diff.repository.DiffRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Base64;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class DiffControllerTestIt {

    private static final int DIFF_ID = 1;
    private static final int NON_EXISTENT_DIFF_ID = 2;
    private static final String TEST_STR = "TEST";
    private static final String BASE_64_TEST_STR = Base64.getEncoder().encodeToString(TEST_STR.getBytes());
    private static final byte[] DIFF_RAW_DATA = BASE_64_TEST_STR.getBytes();
    private static final String ANOTHER_TEST_STR = "DIFFERENT_TEST";
    private static final String ANOTHER_BASE_64_TEST_STR = Base64.getEncoder().encodeToString(ANOTHER_TEST_STR.getBytes());
    private static final byte[] ANOTHER_DIFF_RAW_DATA = ANOTHER_BASE_64_TEST_STR.getBytes();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DiffRepository diffRepository;

    @Test
    public void createRightDiff_withValidRequest_shouldReturnDiff() throws Exception {
        // given
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.empty());
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/right")
                                .content(DIFF_RAW_DATA)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(BASE_64_TEST_STR));

        // then
        verify(diffRepository).findById(DIFF_ID);
        verify(diffRepository).save(any(Diff.class));
    }

    @Test
    public void createRightDiff_withValidRequest_butExistentDiff_shouldReturnDiff() throws Exception {
        // given
        Diff diff = buildDiff();
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/right")
                                .content(DIFF_RAW_DATA)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(BASE_64_TEST_STR));

        // then
        verify(diffRepository).findById(DIFF_ID);
        verify(diffRepository).save(any(Diff.class));
    }

    @Test
    public void createRightDiff_withNullableBody_shouldReturnBadRequest_dueToRequiredBody() throws Exception {
        // given
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.empty());
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/right")
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isBadRequest());

        // then get 400 bad request
    }

    @Test
    public void createRightDiff_withEmptyBody_shouldReturnBadRequest_dueToRequiredBody() throws Exception {
        // given
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.empty());
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/right")
                                .content(new byte[0])
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isBadRequest());

        // then get 400 bad request
    }

    @Test
    public void createLeftDiff_withValidRequest_shouldReturnDiff() throws Exception {
        // given
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.empty());
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/left")
                                .content(DIFF_RAW_DATA)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(BASE_64_TEST_STR));

        // then
        verify(diffRepository).findById(DIFF_ID);
        verify(diffRepository).save(any(Diff.class));
    }

    @Test
    public void createLeftDiff_withValidRequest_butExistentDiff_shouldReturnDiff() throws Exception {
        // given
        Diff diff = buildDiff();
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/left")
                                .content(DIFF_RAW_DATA)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(BASE_64_TEST_STR));

        // then
        verify(diffRepository).findById(DIFF_ID);
        verify(diffRepository).save(any(Diff.class));
    }

    @Test
    public void createLeftDiff_withNullableBody_shouldReturnBadRequest_dueToRequiredBody() throws Exception {
        // given
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.empty());
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/left")
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isBadRequest());

        // then get 400 bad request
    }

    @Test
    public void createLeftDiff_withEmptyBody_shouldReturnBadRequest_dueToRequiredBody() throws Exception {
        // given
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.empty());
        given(diffRepository.save(any(Diff.class))).willReturn(buildDiff());

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID + "/left")
                                .content(new byte[0])
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isBadRequest());

        // then get 400 bad request
    }

    @Test
    public void diff_withValidDiff_shouldReturnSuccess() throws Exception {
        // given
        Diff diff = buildDiff();
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID)
                                .content(DIFF_RAW_DATA)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        // then
        verify(diffRepository).findById(DIFF_ID);
    }

    @Test
    public void diff_withInvalidDiff_shouldReturnNotFound() throws Exception {
        // given
        given(diffRepository.findById(NON_EXISTENT_DIFF_ID)).willReturn(Optional.empty());

        // when
        mockMvc.perform(post("/v1/diff/" + NON_EXISTENT_DIFF_ID)
                                .content(DIFF_RAW_DATA)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));


        // then
        verify(diffRepository).findById(NON_EXISTENT_DIFF_ID);
    }

    @Test
    public void diff_withDifferentRightAndLeft_shouldReturnBadRequest() throws Exception {
        // given
        Diff diff = buildDiff();
        diff.setRightRawData(ANOTHER_DIFF_RAW_DATA);
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        mockMvc.perform(post("/v1/diff/" + DIFF_ID)
                                .content(DIFF_RAW_DATA)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));


        // then
        verify(diffRepository).findById(DIFF_ID);
    }

    private Diff buildDiff() {
        return new Diff.Builder()
                .id(DIFF_ID)
                .leftRawData(DIFF_RAW_DATA)
                .leftParsedData(BASE_64_TEST_STR)
                .rightRawData(DIFF_RAW_DATA)
                .rightParsedData(BASE_64_TEST_STR)
                .build();
    }
}
