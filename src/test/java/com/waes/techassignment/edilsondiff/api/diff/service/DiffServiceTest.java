package com.waes.techassignment.edilsondiff.api.diff.service;

import com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiException;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffResponse;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffVo;
import com.waes.techassignment.edilsondiff.persistence.diff.model.Diff;
import com.waes.techassignment.edilsondiff.persistence.diff.repository.DiffRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class DiffServiceTest {

    private static final int DIFF_ID = 1;
    private static final byte[] DIFF_RAW_DATA = new byte[] {2, 1, 'A'};
    private static final String DIFF_PARSED_VALUE = "DIFF";

    @Mock
    private DiffBuilder diffBuilder;

    @Mock
    private DiffParseHelper diffParseHelper;

    @Mock
    private DiffRepository diffRepository;

    @InjectMocks
    private DiffService diffService;

    @Test
    public void createLeftDiff_withNotFoundDiff_shouldCreateADiffWithLeftSet() {
        // given
        DiffVo diffVo = buildDiffVo();
        Diff leftDiff = buildLeftDiff();
        given(diffRepository.findById(diffVo.getDiffId())).willReturn(Optional.empty());
        given(diffBuilder.buildLeftDiff(diffVo)).willReturn(leftDiff);
        given(diffRepository.save(leftDiff)).willReturn(leftDiff);

        // when
        diffService.createLeftDiff(diffVo);

        // then
        inOrder(diffRepository, diffBuilder);
        verify(diffRepository).findById(diffVo.getDiffId());
        verify(diffBuilder).buildLeftDiff(diffVo);
        verify(diffRepository).save(leftDiff);
        verifyNoMoreInteractions(diffRepository, diffBuilder);
    }

    @Test
    public void createLeftDiff_withExistentDiff_shouldSetLeftDiff() {
        // given
        DiffVo diffVo = buildDiffVo();
        Diff diff = buildRightDiff();
        given(diffRepository.findById(diffVo.getDiffId())).willReturn(Optional.of(diff));
        given(diffParseHelper.parseDiffValue(diffVo.getRawData())).willReturn(DIFF_PARSED_VALUE);
        given(diffRepository.save(diff)).willReturn(diff);

        // when
        diffService.createLeftDiff(diffVo);

        // then
        inOrder(diffRepository, diffParseHelper);
        verify(diffRepository).findById(diffVo.getDiffId());
        verify(diffParseHelper).parseDiffValue(diffVo.getRawData());
        verify(diffRepository).save(diff);
        verifyNoMoreInteractions(diffRepository, diffParseHelper);
    }

    @Test
    public void createRightDiff_withNotFoundDiff_shouldCreateADiffWithRightSet() {
        // given
        DiffVo diffVo = buildDiffVo();
        Diff diff = buildLeftDiff();
        given(diffRepository.findById(diffVo.getDiffId())).willReturn(Optional.empty());
        given(diffBuilder.buildRightDiff(diffVo)).willReturn(diff);
        given(diffRepository.save(diff)).willReturn(diff);

        // when
        diffService.createRightDiff(diffVo);

        // then
        inOrder(diffRepository, diffBuilder);
        verify(diffRepository).findById(diffVo.getDiffId());
        verify(diffBuilder).buildRightDiff(diffVo);
        verify(diffRepository).save(diff);
        verifyNoMoreInteractions(diffRepository, diffBuilder);
    }

    @Test
    public void createRightDiff_withExistentDiff_shouldSeRightDiff() {
        // given
        DiffVo diffVo = buildDiffVo();
        Diff diff = buildLeftDiff();
        given(diffRepository.findById(diffVo.getDiffId())).willReturn(Optional.of(diff));
        given(diffParseHelper.parseDiffValue(diffVo.getRawData())).willReturn(DIFF_PARSED_VALUE);
        given(diffRepository.save(diff)).willReturn(diff);

        // when
        diffService.createRightDiff(diffVo);

        // then
        inOrder(diffRepository, diffParseHelper);
        verify(diffRepository).findById(diffVo.getDiffId());
        verify(diffParseHelper).parseDiffValue(diffVo.getRawData());
        verify(diffRepository).save(diff);
        verifyNoMoreInteractions(diffRepository, diffParseHelper);
    }

    @Test
    public void diff_withValidDiff_shouldReturnDiffResponse() throws DiffApiException {
        // given
        Diff diff = buildFullDiff();
        DiffResponse diffResponse = buildDiffResponse();
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));
        given(diffBuilder.buildDiffResponse(diff)).willReturn(diffResponse);

        // when
        DiffResponse result = diffService.diff(DIFF_ID);

        // then
        assertThat(result.getDiff(), equalTo(diffResponse.getDiff()));
        assertThat(result.getRight(), equalTo(diffResponse.getRight()));
        assertThat(result.getLeft(), equalTo(diffResponse.getLeft()));
    }

    @Test(expected = DiffApiException.class)
    public void diff_withNonExistentDiff_shouldThrowsDiffApiException() throws DiffApiException {
        // given
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.empty());

        // when
        diffService.diff(DIFF_ID);

        // then throw exception
    }

    @Test(expected = DiffApiException.class)
    public void diff_withNullableLeftDiff_shouldThrowDiffApiException() throws DiffApiException {
        // given
        Diff diff = buildFullDiff();
        diff.setLeftRawData(null);
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        diffService.diff(DIFF_ID);

        // then throw exception
    }

    @Test(expected = DiffApiException.class)
    public void diff_withEmptyLeftDiff_shouldThrowDiffApiException() throws DiffApiException {
        // given
        Diff diff = buildFullDiff();
        diff.setLeftRawData(new byte[0]);
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        diffService.diff(DIFF_ID);

        // then throw exception
    }


    @Test(expected = DiffApiException.class)
    public void diff_withNullableRightDiff_shouldThrowDiffApiException() throws DiffApiException {
        // given
        Diff diff = buildFullDiff();
        diff.setRightRawData(null);
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        diffService.diff(DIFF_ID);

        // then throw exception
    }

    @Test(expected = DiffApiException.class)
    public void diff_withEmptyRightDiff_shouldThrowDiffApiException() throws DiffApiException {
        // given
        Diff diff = buildFullDiff();
        diff.setRightRawData(new byte[0]);
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        diffService.diff(DIFF_ID);

        // then throw exception
    }

    @Test(expected = DiffApiException.class)
    public void diff_withDifferentDiff_butSameSize_shouldThrowDiffApiException() throws DiffApiException {
        // given
        Diff diff = buildFullDiff();
        diff.setRightRawData(new byte[] {3, 2, 1});
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        diffService.diff(DIFF_ID);

        // then throw exception
    }

    @Test(expected = DiffApiException.class)
    public void diff_withDifferentDiff_andDifferentSize_shouldThrowDiffApiException() throws DiffApiException {
        // given
        Diff diff = buildFullDiff();
        diff.setRightRawData(new byte[] {3, 2});
        given(diffRepository.findById(DIFF_ID)).willReturn(Optional.of(diff));

        // when
        diffService.diff(DIFF_ID);

        // then throw exception
    }

    private DiffVo buildDiffVo() {
        return new DiffVo.Builder()
                .diffId(DIFF_ID)
                .rawData(DIFF_RAW_DATA)
                .build();
    }

    private Diff buildLeftDiff() {
        return new Diff.Builder()
                .id(DIFF_ID)
                .leftRawData(DIFF_RAW_DATA)
                .leftParsedData(DIFF_PARSED_VALUE)
                .build();
    }

    private Diff buildRightDiff() {
        return new Diff.Builder()
                .id(DIFF_ID)
                .rightRawData(DIFF_RAW_DATA)
                .rightParsedData(DIFF_PARSED_VALUE)
                .build();
    }

    private Diff buildFullDiff() {
        return new Diff.Builder()
                .id(DIFF_ID)
                .leftRawData(DIFF_RAW_DATA)
                .leftParsedData(DIFF_PARSED_VALUE)
                .rightRawData(DIFF_RAW_DATA)
                .rightParsedData(DIFF_PARSED_VALUE)
                .build();
    }

    private DiffResponse buildDiffResponse() {
        return new DiffResponse.Builder()
                .diff(DIFF_ID)
                .right(DIFF_RAW_DATA)
                .left(DIFF_RAW_DATA)
                .build();
    }
}
