package com.waes.techassignment.edilsondiff.api.diff.controller;

import com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiException;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffResponse;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffVo;
import com.waes.techassignment.edilsondiff.api.diff.service.DiffBuilder;
import com.waes.techassignment.edilsondiff.api.diff.service.DiffService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@RunWith(MockitoJUnitRunner.class)
public class DiffControllerTest {

    private static final int DIFF_ID = 1;
    private static final byte[] DIFF_RAW_DATA = new byte[] {2, 1, 'A'};

    @Mock
    private DiffBuilder diffBuilder;

    @Mock
    private DiffService diffService;

    @InjectMocks
    private DiffController diffController;

    @Test
    public void createLeftDiff_withValidRequest_shouldCreateDiff() {
        // given
        DiffVo diffVo = buildDiffVo();
        given(diffBuilder.buildDiffVo(DIFF_ID, DIFF_RAW_DATA)).willReturn(diffVo);
        doNothing().when(diffService).createLeftDiff(diffVo);

        // when
        ResponseEntity<?> result = diffController.createLeftDiff(DIFF_ID, DIFF_RAW_DATA);

        // then
        assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(result.getBody(), equalTo(DIFF_RAW_DATA));
    }

    @Test
    public void createRightDiff_withValidRequest_shouldCreateDiff() {
        // given
        DiffVo diffVo = buildDiffVo();
        given(diffBuilder.buildDiffVo(DIFF_ID, DIFF_RAW_DATA)).willReturn(diffVo);
        doNothing().when(diffService).createRightDiff(diffVo);

        // when
        ResponseEntity<?> result = diffController.createRightDiff(DIFF_ID, DIFF_RAW_DATA);

        // then
        assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(result.getBody(), equalTo(DIFF_RAW_DATA));
    }

    @Test
    public void performDiff_withValidDiffId_shouldMatchesDiffSuccessfully() throws DiffApiException {
        // given
        DiffResponse diffResponse = buildDiffResponse();
        given(diffService.diff(DIFF_ID)).willReturn(diffResponse);

        // when
        ResponseEntity<?> result = diffController.performDiff(DIFF_ID);

        // then
        assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(result.getBody(), equalTo(diffResponse));
    }

    @Test(expected = DiffApiException.class)
    public void performDiff_withInvalidDiffId_shouldThrowsDiffApiException() throws DiffApiException {
        // given
        DiffResponse diffResponse = buildDiffResponse();
        given(diffService.diff(DIFF_ID)).willThrow(DiffApiException.class);

        // when
        diffController.performDiff(DIFF_ID);

        // then should throw exception
    }

    private DiffVo buildDiffVo() {
        return new DiffVo.Builder()
                .diffId(DIFF_ID)
                .rawData(DIFF_RAW_DATA)
                .build();
    }

    private DiffResponse buildDiffResponse() {
        return new DiffResponse.Builder()
                .left(DIFF_RAW_DATA)
                .right(DIFF_RAW_DATA)
                .diff(DIFF_ID)
                .build();
    }

}
