package com.waes.techassignment.edilsondiff.api.diff.model;

public class DiffVo {

    //Class created in order to decouple the controller with the rest of the back-end.
    //So, using this approach if the front-end for some reason added a new attribute the
    //BI level and also the DAO level will not be impacted, since they are using a Immutable VO class
    private int diffId;
    private byte[] rawData;

    private DiffVo() {
    }

    public int getDiffId() {
        return diffId;
    }

    public byte[] getRawData() {
        return rawData;
    }


    public static class Builder {
        private DiffVo diffVo = new DiffVo();

        public Builder diffId(int diifId) {
            this.diffVo.diffId = diifId;
            return this;
        }

        public Builder rawData(byte[] rawData) {
            this.diffVo.rawData = rawData;
            return this;
        }

        public DiffVo build() {
            return this.diffVo;
        }
    }
}
