package com.waes.techassignment.edilsondiff.api.diff.service;

import com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiException;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffResponse;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffVo;
import com.waes.techassignment.edilsondiff.persistence.diff.model.Diff;
import com.waes.techassignment.edilsondiff.persistence.diff.repository.DiffRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.util.Optional;

import static com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiErrorReason.DIFF_NOT_FOUND;
import static com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiErrorReason.INVALID_LEFT_DIFF;
import static com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiErrorReason.INVALID_RIGHT_DIFF;
import static com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiErrorReason.LENGTH_OF_DIFFS_ARE_DIFFERENT;
import static com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiErrorReason.OFFSET_OF_DIFFS_ARE_DIFFERENT;

// This class is the responsible to handle all the requests from {@link DiffController}
@Service
public class DiffService {

    private static final Logger LOG = LoggerFactory.getLogger(DiffService.class);

    private final DiffBuilder diffBuilder;
    private final DiffParseHelper diffParseHelper;
    private final DiffRepository diffRepository;

    public DiffService(DiffBuilder diffBuilder,
                       DiffParseHelper diffParseHelper,
                       DiffRepository diffRepository) {
        this.diffBuilder = diffBuilder;
        this.diffParseHelper = diffParseHelper;
        this.diffRepository = diffRepository; //There is no need to put @Autowired on constructor after the Spring on version 5.3
    }

    public void createLeftDiff(DiffVo diffVo) {

        Diff diff = retrieveDiff(diffVo.getDiffId())
                .map(foundDiff -> setLeftDiff(diffVo.getRawData(), foundDiff))
                .orElseGet(() -> generateLeftDiff(diffVo));

        LOG.debug("Saving the diff due to a left diff update. DiffId: {}.", diffVo.getDiffId());
        diffRepository.save(diff);
    }

    public void createRightDiff(DiffVo diffVo) {

        Diff diff = retrieveDiff(diffVo.getDiffId())
                .map(foundDiff -> setRightDiff(diffVo.getRawData(), foundDiff))
                .orElseGet(() -> generateRightDiff(diffVo));

        LOG.debug("Saving the diff due to a right diff update. DiffId: {}.", diffVo.getDiffId());
        diffRepository.save(diff);
    }

    public DiffResponse diff(int diffId) throws DiffApiException {
        Diff diff = retrieveDiff(diffId)
                .orElseThrow(() -> new DiffApiException(DIFF_NOT_FOUND));

        validateDiff(diff);

        checkDiffs(diff);

        return diffBuilder.buildDiffResponse(diff);
    }

    // Compare the difference between the left and right diff
    private void validateDiff(Diff diff) throws DiffApiException {
        if (diff.getLeftRawData() == null || diff.getLeftRawData().length == 0) {
            LOG.error(INVALID_LEFT_DIFF.getMessage());
            throw new DiffApiException(INVALID_LEFT_DIFF);
        } else if (diff.getRightRawData() == null || diff.getRightRawData().length == 0) {
            LOG.error(INVALID_RIGHT_DIFF.getMessage());
            throw new DiffApiException(INVALID_RIGHT_DIFF);
        }
    }

    private void checkDiffs(Diff diff) throws DiffApiException {
        LOG.info("Starting to perform the diff itself. DiffId: {}.", diff.getId());

        // I am using the MessageDigest.isEqual instead of Arrays.equals since the MessageDigest is safer than the Arrays.equal
        // Have a look in the following article for more details: https://www.pixelstech.net/article/1431658986-Arrays-equals%28%29-vs-MessageDigest-isEqual%28%29
        boolean isMatched = MessageDigest.isEqual(diff.getLeftRawData(), diff.getRightRawData());

        if (!isMatched) {
            if (diff.getRightRawData().length != diff.getLeftRawData().length) {
                LOG.error("The length of right: {} and left: {} are different.", diff.getRightRawData().length, diff.getLeftRawData().length);
                throw new DiffApiException(LENGTH_OF_DIFFS_ARE_DIFFERENT);
            } else {
                LOG.error("The length of right: {} and left: {} are equal but the values are still not the same.", diff.getRightRawData().length, diff.getLeftRawData().length);
                throw new DiffApiException(OFFSET_OF_DIFFS_ARE_DIFFERENT);
            }
        }
    }

    private Diff generateLeftDiff(DiffVo diffVo) {
        LOG.info("There is no Diff created so far. Start to create the new one with the left diff only.");
        return diffBuilder.buildLeftDiff(diffVo);
    }

    private Diff generateRightDiff(DiffVo diffVo) {
        LOG.info("There is no Diff created so far. Start to create a new one with the right diff only.");
        return diffBuilder.buildRightDiff(diffVo);
    }

    // If the diff object was found, only set the left diff
    private Diff setLeftDiff(byte[] rawData, Diff diff) {
        LOG.info("The diff was found, so update the attribute diffLeft. DiffId: {}.", diff.getId());
        String parsedDiff = diffParseHelper.parseDiffValue(rawData);
        diff.setLeftParsedData(parsedDiff);
        diff.setLeftRawData(rawData);

        return diff;
    }

    // If the diff object was found, only set the right diff
    private Diff setRightDiff(byte[] rawData, Diff diff) {
        LOG.info("The diff was found, so update the attribute diffRight. DiffId: {}.", diff.getId());
        String parsedDiff = diffParseHelper.parseDiffValue(rawData);
        diff.setRightParsedData(parsedDiff);
        diff.setRightRawData(rawData);

        return diff;
    }

    // Look into the repository for a diff by a provided ID
    private Optional<Diff> retrieveDiff(int diffId) {
        return diffRepository.findById(diffId);
        
    }
}
