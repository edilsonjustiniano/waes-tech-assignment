package com.waes.techassignment.edilsondiff.api.diff.exception;

import com.waes.techassignment.edilsondiff.exception.BaseExceptionHandler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DiffApiExceptionHandler extends BaseExceptionHandler {

    @ExceptionHandler(DiffApiException.class)
    public ResponseEntity handleError(DiffApiException ex) {
        return this.generateError(ex.getErrorCode(), ex.getMessage(), ex.getHttpStatus());
    }
}
