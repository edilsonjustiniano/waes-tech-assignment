package com.waes.techassignment.edilsondiff.api.diff.model;

public class DiffResponse {

    private int diff;
    private byte[] right;
    private byte[] left;

    public int getDiff() {
        return diff;
    }

    public byte[] getRight() {
        return right;
    }

    public byte[] getLeft() {
        return left;
    }

    public static class Builder {
        private DiffResponse diffResponse = new DiffResponse();

        public Builder diff(int diff) {
            this.diffResponse.diff = diff;
            return this;
        }

        public Builder right(byte[] right) {
            this.diffResponse.right = right;
            return this;
        }

        public Builder left(byte[] left) {
            this.diffResponse.left = left;
            return this;
        }

        public DiffResponse build() {
            return this.diffResponse;
        }
    }
}
