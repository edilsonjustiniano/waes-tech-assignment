package com.waes.techassignment.edilsondiff.api.diff.service;

import org.springframework.stereotype.Service;

import java.util.Base64;

// This class is responsible to parser the raw data received from Front-end to a String value
@Service
public class DiffParseHelper {

    public String parseDiffValue(byte[] rawData) {
        byte[] parsedData = Base64.getDecoder().decode(new String(rawData));

        return new String(parsedData);
    }
}
