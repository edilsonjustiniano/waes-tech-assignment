package com.waes.techassignment.edilsondiff.api.diff.exception;

import com.waes.techassignment.edilsondiff.exception.BaseException;

public class DiffApiException extends BaseException {

    public DiffApiException(DiffApiErrorReason diffApiErrorReason) {
        super(diffApiErrorReason.getErrorCode(), diffApiErrorReason.getMessage(), diffApiErrorReason.getHttpStatus());
    }

}
