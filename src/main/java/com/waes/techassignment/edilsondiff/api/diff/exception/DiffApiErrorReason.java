package com.waes.techassignment.edilsondiff.api.diff.exception;

import org.springframework.http.HttpStatus;

public enum DiffApiErrorReason {

    DIFF_NOT_FOUND("request.diff.notfound", "The provided diff was not found.", HttpStatus.NOT_FOUND),
    INVALID_RIGHT_DIFF("invalid.right.diff", "The provided diff does not contain a right diff set.", HttpStatus.BAD_REQUEST),
    INVALID_LEFT_DIFF("invalid.left.diff", "The provided diff does not contain a left diff set.", HttpStatus.BAD_REQUEST),

    LENGTH_OF_DIFFS_ARE_DIFFERENT("length.diff.different", "The left and right length are different. Their length didn't match.", HttpStatus.BAD_REQUEST),
    OFFSET_OF_DIFFS_ARE_DIFFERENT("offset.diff.different", "The left and right offsets are different.", HttpStatus.BAD_REQUEST);

    //ErrorCode: Used for Front-end applications to replace the message properly. It does also help the app internationalization
    private String errorCode;
    //message: This is the message used by the Back-end to let us know about what happened on the request
    private String message;
    //HttpStatus: To avoid the unnecessary new Exceptions since I can handle all the possible error scenarios using only one with the HTTP status variation only
    private HttpStatus httpStatus;

    DiffApiErrorReason(String errorCode, String message, HttpStatus httpStatus) {
        this.errorCode = errorCode;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
