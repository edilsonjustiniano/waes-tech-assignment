package com.waes.techassignment.edilsondiff.api.diff.service;

import com.waes.techassignment.edilsondiff.api.diff.model.DiffResponse;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffVo;
import com.waes.techassignment.edilsondiff.persistence.diff.model.Diff;
import org.springframework.stereotype.Service;

// This class will be responsible to build any kind of objects regarding of Diff. such as Diff, DiffVo, DiffResponse
@Service
public class DiffBuilder {

    private final DiffParseHelper diffParseHelper;

    public DiffBuilder(DiffParseHelper diffParseHelper) {
        this.diffParseHelper = diffParseHelper;
    }

    public DiffVo buildDiffVo(int id, byte[] rawData) {
        return new DiffVo.Builder()
                .diffId(id)
                .rawData(rawData)
                .build();
    }

    public Diff buildLeftDiff(DiffVo diffVo) {
        String parsedDiff = diffParseHelper.parseDiffValue(diffVo.getRawData());
        return buildDiff(diffVo.getDiffId())
                .leftRawData(diffVo.getRawData())
                .leftParsedData(parsedDiff)
                .build();
    }

    public Diff buildRightDiff(DiffVo diffVo) {
        String parsedDiff = diffParseHelper.parseDiffValue(diffVo.getRawData());
        return buildDiff(diffVo.getDiffId())
                .rightRawData(diffVo.getRawData())
                .rightParsedData(parsedDiff)
                .build();
    }

    public DiffResponse buildDiffResponse(Diff diff) {
        return new DiffResponse.Builder()
                .diff(diff.getId())
                .right(diff.getRightRawData())
                .left(diff.getLeftRawData())
                .build();
    }

    private Diff.Builder buildDiff(int id) {
        return new Diff.Builder().id(id);
    }
}
