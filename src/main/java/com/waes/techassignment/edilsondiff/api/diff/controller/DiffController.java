package com.waes.techassignment.edilsondiff.api.diff.controller;

import com.waes.techassignment.edilsondiff.api.diff.exception.DiffApiException;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffResponse;
import com.waes.techassignment.edilsondiff.api.diff.model.DiffVo;
import com.waes.techassignment.edilsondiff.api.diff.service.DiffBuilder;
import com.waes.techassignment.edilsondiff.api.diff.service.DiffService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class DiffController {

    private static final Logger LOG = LoggerFactory.getLogger(DiffController.class);

    private static final String DIFF_BASE_URL = "/v1/diff/{id}";
    private static final String PATH_PARAM_ID = "id";

    private final DiffBuilder diffBuilder;
    private final DiffService diffService;

    public DiffController(DiffBuilder diffBuilder,
            DiffService diffService) {
        this.diffBuilder = diffBuilder;
        this.diffService = diffService;
    }

    @PostMapping(value = DIFF_BASE_URL + "/left",  consumes = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> createLeftDiff(@PathVariable(PATH_PARAM_ID) int id, @RequestBody  byte[] body) {
        LOG.info("Creating the left diff. DiffId: {}.", id);

        // I have decided to validate the provided body. But, the Spring boot already provided the validation for required body
        // So I won't recreate the wheel. Also the documentation is clearly stating that the body is mandatory
        DiffVo diffVo = diffBuilder.buildDiffVo(id, body);

        // Call the service that will register the left diff
        diffService.createLeftDiff(diffVo);

        return ResponseEntity
                .ok(body);
    }

    @PostMapping(value = DIFF_BASE_URL + "/right", consumes = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> createRightDiff(@PathVariable(PATH_PARAM_ID) int id, @RequestBody byte[] body) {
        LOG.info("Creating the right diff. DiffId: {}.", id);

        // I have decided to validate the provided body. But, the Spring boot already provided the validation for required body
        // So I won't recreate the wheel. Also the documentation is clearly stating that the body is mandatory
        DiffVo diffVo = diffBuilder.buildDiffVo(id, body);

        // Call the service that will register the right diff
        diffService.createRightDiff(diffVo);

        return ResponseEntity
                .ok(body);
    }


    @PostMapping(value = DIFF_BASE_URL)
    public ResponseEntity<?> performDiff(@PathVariable(PATH_PARAM_ID) int id) throws DiffApiException {
        LOG.info("Performing the diff between the right and the left. DiffId: {}.", id);

        // Since there is only a parameter on this endpoint I didn't create the VO object, It does not make much sense
        // So I will send the diffId as parameter for service level

        // Call the service that will perform the diff
        DiffResponse diffResponse = diffService.diff(id);

        return ResponseEntity
                .ok(diffResponse);
    }



}
