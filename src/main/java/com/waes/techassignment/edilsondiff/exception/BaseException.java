package com.waes.techassignment.edilsondiff.exception;

import org.springframework.http.HttpStatus;

//Create a base exception that could be used for all the new exceptions that could be appear in a future
public class BaseException extends Exception {

    private String message;
    private String errorCode;
    private HttpStatus httpStatus;


    public BaseException(String errorCode, String message, HttpStatus httpStatus) {
        this.message = message;
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}