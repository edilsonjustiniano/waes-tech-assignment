package com.waes.techassignment.edilsondiff.exception.model;

//Create a base error to be presented to front-end in a JSON format for any exception
public class BaseError {

    private String message;
    private String errorCode;

    private BaseError() {
    }

    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }


    public static class Builder {

        private BaseError baseError = new BaseError();

        public Builder message(String message) {
            baseError.message = message;
            return this;
        }

        public Builder errorCode(String errorCode) {
            baseError.errorCode = errorCode;
            return this;
        }

        public BaseError build() {
            return baseError;
        }
    }
}
