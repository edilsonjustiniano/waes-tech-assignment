package com.waes.techassignment.edilsondiff.persistence.diff.repository;

import com.waes.techassignment.edilsondiff.persistence.diff.model.Diff;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiffRepository extends MongoRepository<Diff, Integer> {
}
