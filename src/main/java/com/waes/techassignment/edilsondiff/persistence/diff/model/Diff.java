package com.waes.techassignment.edilsondiff.persistence.diff.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Diff {

    @Id
    private int id;

    private byte[] leftRawData;
    private String leftParsedData;

    private byte[] rightRawData;
    private String rightParsedData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getLeftRawData() {
        return leftRawData;
    }

    public void setLeftRawData(byte[] leftRawData) {
        this.leftRawData = leftRawData;
    }

    public String getLeftParsedData() {
        return leftParsedData;
    }

    public void setLeftParsedData(String leftParsedData) {
        this.leftParsedData = leftParsedData;
    }

    public byte[] getRightRawData() {
        return rightRawData;
    }

    public void setRightRawData(byte[] rightRawData) {
        this.rightRawData = rightRawData;
    }

    public String getRightParsedData() {
        return rightParsedData;
    }

    public void setRightParsedData(String rightParsedData) {
        this.rightParsedData = rightParsedData;
    }

    public static class Builder {

        private Diff diff = new Diff();

        public Builder id(int id) {
            this.diff.setId(id);
            return this;
        }

        public Builder leftRawData(byte[] leftRawData) {
            this.diff.setLeftRawData(leftRawData);
            return this;
        }

        public Builder leftParsedData(String leftParsedData) {
            this.diff.setLeftParsedData(leftParsedData);
            return this;
        }

        public Builder rightRawData(byte[] rightRawData) {
            this.diff.setRightRawData(rightRawData);
            return this;
        }

        public Builder rightParsedData(String rightParsedData) {
            this.diff.setRightParsedData(rightParsedData);
            return this;
        }

        public Diff build() {
            return this.diff;
        }
    }
}
